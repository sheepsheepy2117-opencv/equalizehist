# C++ with OpenCV library function : equalizeHist

Before Using :

- Install OpenCV first.  
- The code runs on Visual Studio
- The library is OpenCV 3.44

About equalizeHist :

- [OpenCV equalizeHist function website](https://docs.opencv.org/3.1.0/d4/d1b/tutorial_histogram_equalization.html) 
- [MonkeyCoding 阿洲的程式教學 直方圖等化](http://monkeycoding.com/?tag=equalizehist)

Files:

- readme.md
- equalizeHist-sheepsheepy2117.cpp : write by myself
- equalizeHist.cpp : the one use OpenCV library
- License Copyright (c) 2019 sheepsheepy2117-OpenCV

## Description : equalizeHist-sheepsheepy2117.cpp 

#### changeGray( )
The changeGray( ) get a color image and return a gray one.  

```c++
Mat changeGray(Mat src)
{
	Mat gray = Mat(src.rows, src.cols, CV_8UC1); //open a same size of src Mat & CV_8UC1 equals gray type
	double cal = 0;

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			//Gray = B * 0.114 + G * 0.587 + R * 0.299
			cal = src.at<Vec3b>(i, j)[0] * 0.114; //B
			cal = cal + src.at<Vec3b>(i, j)[1] * 0.587; //G
			cal = cal + src.at<Vec3b>(i, j)[2] * 0.299; //R

			//avoid the value bigger than 255 or smaller than 0
			if (round(cal) < 0) 	//need round() 
				gray.at<uchar>(i, j) = 0;
			else if (round(cal) > 255)
				gray.at<uchar>(i, j) = 255;
			else
				gray.at<uchar>(i, j) = round(cal);
		}
	}

	return gray;
}
```
#### frequencyCDFarr( )

The frequencyCDFarr total all the numbers of every Gray scale (0-255).
```c++
void frequencyCDFarr(Mat src) //count every 0-255
{ 
	int cal;
	for (int k = 0; k < 256; k++)
	{
		cal = 0;
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				if (src.at<uchar>(i, j) == k)
					cal++;
			}
		}
		frequencyarr[k] = cal;
	}
}
```
#### accumulateCDFarr( )
AccumulateCDFarr total all the Gray scale.
```c++
void accumulateCDFarr(Mat src) //add every 0-255
{ 
	int cal = 0;
	for (int i = 0; i < 256; i++)
	{
		accumulatearr[i] = cal + frequencyarr[i];
		cal = accumulatearr[i];
	}
}
```
#### drawHistogram( )
Draw the line Chart.
```c++
Mat drawHistogram() //draw the Histogram
{ 
	Mat histogram(256, 256, CV_8U, Scalar(255)); //open a 256*256的Mat

	int maxnum = frequencyarr[0];
	for (int i = 0; i < 256; i++)
	{
		if (frequencyarr[i] > maxnum)
			maxnum = frequencyarr[i]; //get the max value
	}

	for (int i = 0; i < 256; i++)
	{ //use line to draw Histogram
		line(histogram, Point(i, 255), Point(i, 255 - round(frequencyarr[i] * ((0.9 * 256) / maxnum))), Scalar(0)); // make it clear
	}
	return histogram;
}
```
#### Histogramequalization( )
```c++
Mat Histogramequalization(Mat src)
{ 
	Mat drt = Mat(src.rows, src.cols, CV_8UC1); //Open a same size Mat
	int data, newdata;
	int cdfmin = CDFmin(); //get the min value
	int rownum = src.rows; //get the src hight & length
	int colnum = src.cols;
	for (int i = 0; i < rownum; i++)
	{
		for (int j = 0; j < colnum; j++)
		{
			data = src.at<uchar>(i, j); //get the original value
			drt.at<uchar>(i, j) = ((accumulatearr[data] - cdfmin) * 255 / ((rownum * colnum) - cdfmin)); //after calculate put back to the new Mat
		}
	}
	return drt;
}
```
----------
![input](input.jpg "Inputimage") 
![grayinput](grayinput.jpg "Grayinput") 
![HistogramA](HistogramA.jpg "HistogramInput") 
![output](output.jpg "Outputimage") 
![HistogramB](HistogramB.jpg "HistogramOutput") 
![OutputbyOpencv](imgD.jpg "OutputbyOpencv") 

## LICENSE
Copyright (c) 2019 sheepsheepy2117-OpenCV